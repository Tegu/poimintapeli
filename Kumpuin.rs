// Testattu Rust-versiolla 1.31.1. Kääntyy komennolla 'rustc kumpuin.rs'.

use std::io::Write;

#[derive(Clone, Debug)]
enum Move {
    North,
    East,
    South,
    West,
    Eliminated,
}

#[derive(Clone, Debug)]
enum NodeColor {
    White,
    Gray,
    Black,
}

struct Level {
    size: u32,
    data: Vec<Vec<u32>>,
}

#[derive(Clone, Debug)]
struct Position {
    x: i32,
    y: i32,
}

#[derive(Debug)]
struct Player {
    position: Position,
    score: u32,
}

fn read_player_count() -> u32 {
    let mut count = String::new();
    std::io::stdin()
        .read_line(&mut count)
        .expect("Failed to read player count");
    let count: u32 = count
        .trim()
        .parse()
        .expect("Player count input was not a number");
    count
}

fn parse_row(line: String) -> Vec<u32> {
    let mut row = Vec::new();
    for c in line.trim().chars() {
        let score = c.to_digit(10).expect("Failed to convert number");
        row.push(score);
    }
    row
}

fn read_level(size: u32) -> Level {
    let mut data = Vec::new();
    for _ in 0..size {
        let mut line = String::new();
        std::io::stdin().read_line(&mut line).expect("Invalid line");
        if line == "" {
            break;
        }
        let row = parse_row(line);
        data.push(row);
    }
    Level {
        size: size,
        data: data,
    }
}

fn parse_move(c: char) -> Option<Move> {
    match c {
        'w' => Some(Move::North),
        'a' => Some(Move::West),
        's' => Some(Move::South),
        'd' => Some(Move::East),
        '0' => Some(Move::Eliminated),
        _ => None,
    }
}

fn print_move(mov: &Move) {
    let letter = match mov {
        Move::North => 'w',
        Move::West => 'a',
        Move::South => 's',
        Move::East => 'd',
        Move::Eliminated => '0',
    };
    println!("{}", letter);
    std::io::stdout().flush().expect("Failed to flush stdout");
}

fn read_moves(count: u32) -> Option<Vec<Move>> {
    let mut line = String::new();
    std::io::stdin()
        .read_line(&mut line)
        .expect("Failed to read others' moves");
    if line == "" || line.trim() == "-" {
        return None;
    }
    let mut moves = Vec::<Move>::new();
    for c in line.trim().chars().take(count as usize) {
        let mov = parse_move(c).expect("Invalid move");
        moves.push(mov);
    }
    assert_eq!(moves.len(), count as usize);
    Some(moves)
}

fn move_position(pos: &Position, mov: &Move, level_size: u32) -> Position {
    let size = level_size as i32;
    match mov {
        Move::North => Position {
            x: pos.x,
            y: (pos.y + size - 1) % size,
        },
        Move::East => Position {
            x: (pos.x + 1) % size,
            y: pos.y,
        },
        Move::South => Position {
            x: pos.x,
            y: (pos.y + 1) % size,
        },
        Move::West => Position {
            x: (pos.x + size - 1) % size,
            y: pos.y,
        },
        Move::Eliminated => Position { x: pos.x, y: pos.y },
    }
}

fn move_player(player: &mut Player, mov: &Move, level_size: u32) {
    let new_position = move_position(&player.position, &mov, level_size);
    player.position = new_position;
}

fn create_players(player_count: u32, size: u32) -> Vec<Player> {
    let mut players = Vec::<Player>::new();
    for _ in 0..player_count {
        let player = Player {
            position: Position {
                x: size as i32 / 2,
                y: size as i32 / 2,
            },
            score: 0,
        };
        players.push(player);
    }
    players
}

fn _eprint_level(level: &Level) {
    for row in level.data.iter() {
        for c in row.iter() {
            if *c != 0 {
                eprint!("{}", c);
            } else {
                eprint!(".");
            }
        }
        eprintln!("");
    }
}

fn update_player_scores(level: &Level, players: &mut Vec<Player>) {
    for player in players {
        let x = player.position.x as usize;
        let y = player.position.y as usize;
        assert!(x < (level.size as usize));
        assert!(y < (level.size as usize));
        player.score += level.data[y][x];
        //eprintln!("Updated ({}, {}) {}", x, y, player.score);
    }
}

fn clear_level_scores(level: &mut Level, players: &Vec<Player>) {
    for player in players {
        let x = player.position.x as usize;
        let y = player.position.y as usize;
        assert!(x < (level.size as usize));
        assert!(y < (level.size as usize));
        level.data[y][x] = 0;
    }
}

fn opposite_move(mov: &Move) -> Move {
    match mov {
        Move::North => Move::South,
        Move::East => Move::West,
        Move::South => Move::North,
        Move::West => Move::East,
        Move::Eliminated => Move::Eliminated,
    }
}

fn backtrack_moves(previous: &Vec<Vec<Option<Move>>>, start: &Position, level_size: u32) -> Move {
    let mut pos = start.clone();
    while let Some(ref mov) = previous[pos.y as usize][pos.x as usize] {
        //eprintln!("{:?} {:?}", pos, mov);
        let new_pos = move_position(&pos, mov, level_size);
        if previous[new_pos.y as usize][new_pos.x as usize].is_some() {
            pos = new_pos;
        } else {
            return opposite_move(mov);
        }
    }
    Move::Eliminated
}

fn decide_move(level: &Level, position: &Position) -> Move {
    let size = level.size as usize;
    let possible_moves = [Move::North, Move::East, Move::South, Move::West];
    let mut previous: Vec<Vec<Option<Move>>> = vec![vec![None; size]; size];
    let mut cumulative: Vec<Vec<u32>> = vec![vec![0; size]; size];
    let mut distances: Vec<Vec<u32>> = vec![vec![0; size]; size];
    let mut visited: Vec<Vec<NodeColor>> = vec![vec![NodeColor::White; size]; size];
    let mut queue = std::collections::VecDeque::<Position>::new();
    queue.push_back(position.clone());
    while !queue.is_empty() {
        let pos = queue.pop_front().unwrap();
        visited[pos.y as usize][pos.x as usize] = NodeColor::Black;
        for mov in possible_moves.iter() {
            let new_pos = move_position(&pos, mov, size as u32);
            let nx = new_pos.x as usize;
            let ny = new_pos.y as usize;
            let distance = distances[pos.y as usize][pos.x as usize] + 1;
            let new_score = cumulative[pos.y as usize][pos.x as usize]
                + (level.data[ny][nx] * level.size / (distance + 1));
            match visited[ny][nx] {
                NodeColor::White => {
                    visited[ny][nx] = NodeColor::Gray;
                    distances[ny][nx] = distance;
                    cumulative[ny][nx] = new_score;
                    let opposite = opposite_move(mov);
                    previous[ny][nx] = Some(opposite);
                    queue.push_back(new_pos);
                }
                NodeColor::Gray => {
                    if new_score > cumulative[ny][nx] && distance <= distances[ny][nx] {
                        distances[ny][nx] = distance;
                        cumulative[ny][nx] = new_score;
                        let opposite = opposite_move(mov);
                        previous[ny][nx] = Some(opposite);
                    }
                }
                NodeColor::Black => (),
            }
        }
    }
    //for row in cumulative.iter() {
    //    for col in row.iter() {
    //        eprint!("{: <4}", col);
    //    }
    //    eprint!("\n");
    //}
    let max_item = cumulative
        .iter()
        .enumerate()
        .flat_map(|(y, v)| v.iter().enumerate().map(move |(x, v)| (x, y, v)))
        .max_by_key(|item| item.2);
    if let Some((x, y, _)) = max_item {
        let start_pos = Position {
            x: x as i32,
            y: y as i32,
        };
        let mov = backtrack_moves(&previous, &start_pos, level.size);
        //eprintln!("Chosen move: {:?}", mov);
        return mov;
    } else {
        return Move::North;
    }
}

fn main() {
    const NAME: &str = "Kumpuin";
    println!("{}", NAME);
    let player_count = read_player_count();
    //eprintln!("Player count {}", player_count);
    let mut level = read_level(25);
    //eprintln!("Level size: {0} x {0}", level.size);
    let mut players = create_players(player_count, level.size);
    //_eprint_level(&level);
    loop {
        let own_move = decide_move(&level, &players[0].position);
        print_move(&own_move);
        move_player(&mut players[0], &own_move, level.size);
        let moves = read_moves(player_count - 1);
        if let Some(ms) = moves {
            //eprintln!("Others' moves: {:?}", ms);
            //eprintln!("{:?}", players);
            for (i, m) in ms.iter().enumerate() {
                move_player(&mut players[i + 1], m, level.size);
            }
            //eprintln!("{:?}", players);
            update_player_scores(&level, &mut players);
            clear_level_scores(&mut level, &players);
            //eprint_level(&level);
        } else {
            break; // Game ended
        }
    }
}
